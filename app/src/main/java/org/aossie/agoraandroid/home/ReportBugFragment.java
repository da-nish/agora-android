package org.aossie.agoraandroid.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import org.aossie.agoraandroid.R;
import org.aossie.agoraandroid.WebContent;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportBugFragment extends Fragment {

  public ReportBugFragment() {
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_report_bug, null);
    Button openConsole = view.findViewById(R.id.button_report_bug);
    openConsole.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(getContext(), WebContent.class).putExtra("url","https://gitlab.com/aossie/agora-android/issues/new"));

        //  old method - redirecting to browser
        //
        //        startActivity(new Intent(Intent.ACTION_VIEW,
        //            Uri.parse("https://gitlab.com/aossie/agora-android/issues/new")));

      }
    });
    return view;
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }
}
